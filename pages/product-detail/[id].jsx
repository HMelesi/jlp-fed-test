import ProductCarousel from "../../components/product-carousel/product-carousel";
import styles from "./product-detail.module.scss";
import { readFile } from "fs/promises";
import Link from "next/link";

export async function getServerSideProps(context) {
  const env = process.env.NODE_ENV || "development";

  let data;

  if (env === "test" || env === "development") {
    const response = await readFile("mockData/data2.json", "utf-8");
    data = JSON.parse(response);
  } else {
    const id = context.params.id;
    const response = await fetch(
      "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id
    );
    data = await response.json();
  }

  return {
    props: { data },
  };
}

const ProductDetail = ({ data }) => {
  const product = data.products[0];
  return (
    <div className={styles.content}>
      <header className={styles.nav}>
        <Link
          href={{
            pathname: "/",
          }}
        >
          <a className={styles.backButton}>&lsaquo;</a>
        </Link>
        <h1 className={styles.productTitle}>{product.title}</h1>
      </header>
      <div className={styles.contentBlock}>
        <ProductCarousel images={product.media.images} />
      </div>
      <div className={styles.priceBlock}>
        <div className={styles.priceBlockInner}>
          <h2 className={styles.price}>£{product.price.now}</h2>
          <p className={styles.specialOffer}>{product.displaySpecialOffer}</p>
          <p className={styles.includedServices}>
            {product.additionalServices.includedServices}
          </p>
        </div>
      </div>
      <div className={styles.infoBlock}>
        <h3 className={styles.infoHeader}>Product information</h3>
        <div
          className={styles.infoContent}
          dangerouslySetInnerHTML={{
            __html: product.details.productInformation,
          }}
        ></div>
        <div className={styles.readMore}>
          <p>Read more</p>
          <p>&rsaquo;</p>
        </div>
        <p className={styles.productCode}>
          Product code: {product.skus[0].code}
        </p>
        <div className={styles.specContent}>
          <h3 className={styles.infoHeader}>Product specification</h3>
          <ul className={styles.infoList}>
            {product.details.features[0].attributes.map((item) => (
              <li className={styles.infoRow} key={item.id}>
                <p>{item.name}</p>
                <p className={styles.infoRowValue}>{item.value}</p>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
