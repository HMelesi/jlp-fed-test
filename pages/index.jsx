import Head from "next/head";
import Link from "next/link";
import styles from "./index.module.scss";
import ProductListItem from "../components/product-list-item/product-list-item";
import { readFile } from "fs/promises";
import { chunk } from "lodash";
import { useState } from "react";

export async function getServerSideProps() {
  const env = process.env.NODE_ENV || "development";
  let data;

  if (env === "test" || env === "development") {
    const response = await readFile("mockData/data.json", "utf-8");
    data = JSON.parse(response);
  } else {
    const response = await fetch(
      "https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI"
    );
    data = await response.json();
  }

  return {
    props: {
      data: data,
    },
  };
}

const Home = ({ data }) => {
  const items = chunk(data.products, 20);
  const [page, setPage] = useState(0);

  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div className={styles.page}>
        <header className={styles.pageHeader}>
          <h1>Dishwashers {`(${data.products.length})`}</h1>
        </header>
        <div className={styles.content}>
          {items[page].map((item) => (
            <Link
              key={item.productId}
              href={{
                pathname: "/product-detail/[id]",
                query: { id: item.productId },
              }}
            >
              <a className={styles.link}>
                <ProductListItem
                  imageAlt={item.title}
                  image={item.image}
                  description={item.title}
                  price={item.price.now}
                />
              </a>
            </Link>
          ))}
        </div>
        <div className={styles.pageButtons}>
          {page > 0 ? (
            <button
              data-testid="previous-button"
              onClick={() => {
                setPage((currentPage) => {
                  return currentPage - 1;
                });
              }}
            >
              &lsaquo;
            </button>
          ) : (
            <div />
          )}
          {page < items.length - 1 ? (
            <button
              data-testid="next-button"
              onClick={() => {
                setPage((currentPage) => {
                  return currentPage + 1;
                });
              }}
            >
              &rsaquo;
            </button>
          ) : (
            <div />
          )}
        </div>
      </div>
    </div>
  );
};

export default Home;
