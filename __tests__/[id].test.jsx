import "@testing-library/jest-dom";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { readFile } from "fs/promises";
import ProductDetail from "../pages/product-detail/[id].jsx";

describe("ProductDetail", () => {
  let data;

  beforeAll(async () => {
    const response = await readFile(
      `${__dirname}/../mockData/data2.json`,
      "utf-8"
    );
    data = JSON.parse(response);
  });

  it("renders Dishwasher heading with the title of the dishwasher", async () => {
    render(<ProductDetail data={data} />);

    const heading = await screen.findByText(
      "Bosch Serie 2 SMS24AW01G Freestanding Dishwasher, White"
    );

    expect(heading).toBeInTheDocument();
  });

  it("renders an image of the dishwasher", async () => {
    render(<ProductDetail data={data} />);

    const dishwasherImage = await screen.findByTestId("product-detail-image");

    expect(dishwasherImage.src).toBe(
      "http://johnlewis.scene7.com/is/image/JohnLewis/236888513?"
    );
  });

  it("renders the dishwasher price, special offer and included services", async () => {
    render(<ProductDetail data={data} />);

    const price = await screen.findByText("£329.00");

    const specialOffer = price.nextSibling;

    expect(specialOffer.textContent).toBe(
      "Claim an extra 3 years guarantee via redemption"
    );

    const additionalServices = specialOffer.nextSibling;

    expect(additionalServices.textContent).toBe("2 year guarantee included");
  });

  it("renders product information for the dishwasher", async () => {
    render(<ProductDetail data={data} />);

    await screen.findByText("Product information");

    screen.getByText("Product code: 81701106");

    screen.getByText("Product specification");

    data.products[0].details.features[0].attributes.forEach((attribute) => {
      screen.getByText(attribute.name);
    });
  });
});
