import "@testing-library/jest-dom";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { readFile } from "fs/promises";
import Home from "../pages/index";

describe("Home", () => {
  let data;

  beforeAll(async () => {
    const response = await readFile(
      `${__dirname}/../mockData/data.json`,
      "utf-8"
    );
    data = JSON.parse(response);
  });

  it("renders Dishwashers heading with number of dishwashers", async () => {
    render(<Home data={data} />);

    const heading = await screen.findByText("Dishwashers (24)");

    expect(heading).toBeInTheDocument();
  });

  it("renders only 20 dishwashers on the page", async () => {
    render(<Home data={data} />);

    const dishwasherItems = await screen.findAllByTestId("product-list-item");

    expect(dishwasherItems.length).toBe(20);
  });

  it("renders each dishwasher with an image, description and price", async () => {
    render(<Home data={data} />);

    const dishwasherItems = await screen.findAllByTestId("product-list-item");

    dishwasherItems.forEach((dishwasherItem, index) => {
      const itemData = data.products[index];
      expect(dishwasherItem.children.length).toBe(3);
      expect(dishwasherItem.children[0].firstChild.src).toBe(
        `http:${itemData.image}`
      );
      expect(dishwasherItem.children[1].textContent).toBe(itemData.title);
      expect(dishwasherItem.children[2].textContent).toBe(
        `£${itemData.price.now}`
      );
    });
  });

  it("renders a next and previous buttons, which when click displays next/previous 20 dishwashers", async () => {
    render(<Home data={data} />);

    const nextButton = await screen.findByTestId("next-button");

    await waitFor(() => fireEvent.click(nextButton));

    const pageTwoDishwasherItems = await screen.findAllByTestId(
      "product-list-item"
    );

    expect(pageTwoDishwasherItems.length).toBe(4);

    const previousButton = await screen.findByTestId("previous-button");

    await waitFor(() => fireEvent.click(previousButton));

    const pageOneDishwasherItems = await screen.findAllByTestId(
      "product-list-item"
    );

    expect(pageOneDishwasherItems.length).toBe(20);
  });
});
