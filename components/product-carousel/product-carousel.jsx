import styles from "./product-carousel.module.scss";

const ProductCarousel = ({ images }) => {
  return (
    <div className={styles.productCarousel}>
      <img
        data-testid="product-detail-image"
        src={images.urls[0]}
        alt={images.altText}
        style={{ width: "100%", maxWidth: "500px" }}
      />
    </div>
  );
};

export default ProductCarousel;
