import styles from "./product-list-item.module.scss";

const ProductListItem = ({ imageAlt, image, price, description }) => {
  return (
    <div data-testid="product-list-item" className={styles.content}>
      <div className={styles.imgContainer}>
        <img className={styles.img} src={image} alt={imageAlt} />
      </div>
      <p className={styles.desc}>{description}</p>
      <p className={styles.price}>£{price}</p>
    </div>
  );
};

export default ProductListItem;
