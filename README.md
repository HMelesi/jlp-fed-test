# UI Dev Technical Test - Dishwasher App

## Notes/observations

- README indicates it shoud run on port 3000, but scripts use 3001. Have changed to use 3000.
- API appears to be inaccessible when accessing from http://localhost:3000, although this is just timing out and not providing an error message. I can only assume http or localhost requests are blocked by the API (I tested other open APIs in the code and they worked fine). Solution used for this:
  - Use the test data to implement the designs
  - Code written so data source is based on environment (test and dev use test data, production would hypothetically use the API).
- Edited test data in data2.json to have key of "products" rather than "detailsData" to reflect the actual response from the API.
- Removed use of dangerouslySetInnerHTML as potential to expose users to XSS.
- Have assumed the id on an attribute is unique (to use for a key).
- Added a displaySpecialOffer to test data as was an empty string.
- productInformation is written in html, meaning for now I've used dangerouslySetInnerHTML; this isn't ideal, but I couldn't find a non-HTML version. I would suggest this is stored as plain text or even markdown. Product information doesn't seem to exist on the API endpoint.
- I rolled back the version of @testing-library/react as it didn't seem so compatible with React 17.
- Testing has been done on page level in **tests**, this is not as thorough as I would like but I had limited time to complete this along with styling.

## Designs:

- Background colour on images in test data doesn't match those on designs, have ignored this issue.
- Made some guesses with colours using a colour picker to get the hex from the designs - think they're pretty close, but obviously in an ideal world would have the hex codes or similar.
- No designs for different screen sizes (eg extra large screens) so made some judgements on what looked better spacing/font size wise.
- There were no styles supplied for navigating between pages of dishwashers. I'm not a designer but I've just used similar chevrons to those in the rest of the designs.
- Using plain CSS takes a long time! I'd probably prefer to use tailwind or similar, althoug I appreciate the flexibility of pure CSS it does take a lot longer.
- Exact colours/font sizes would be good when building, but I think I've got it close enough.
